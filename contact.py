#!/usr/bin/env python3
"""Contact manager"""

from pprint import pprint


class Contact:
    """Contact name tracker"""

    def __init__(self):
        # The tree is a dict where the character is the key, and it contains a
        # list: [{children}, count]
        self.tree = {}

    def add_name(self, name):
        """Added a name to the contact manager"""

        if name == "":
            return 0

        return self.add_substring(self.tree, name)

    def add_substring(self, tree, name):
        """Adds a substring to a tree"""

        # If the name is in the tree already, it's a single character, so
        # increment the count and return the new count.
        if name in tree:
            tree[name][1] += 1
            return tree[name][1]

        # If there's only one character in the substring, this is the end of
        # the name
        if len(name) == 1:
            # It's not in the tree, so initialize with a new
            # dictionary to track children, and a count of 1.
            tree[name] = [{}, 1]
            return 1

        # If this substring hasn't been added to the tree before, initialize a
        # dict for its children
        if name[0] not in tree:
            tree[name[0]] = [{}, 0]

        # Add the rest of the string to the tree
        return self.add_substring(tree[name[0]][0], name[1:])

    def count_names(self, tree):
        """Counts the names in the section of the tree"""

        total = 0

        for node in tree.values():
            total += node[1]
            children = node[0]

            if not children:
                continue

            for subtree in children.values():
                total += subtree[1]
                if subtree[0]:
                    subtotal = self.count_names(subtree[0])
                    total += subtotal

        return total

    def find_subtree(self, tree, prefix):
        """Finds a subtree with the prefix"""

        if prefix in tree:
            return tree

        if prefix[0] in tree:
            return self.find_subtree(tree[prefix[0]][0], prefix[1:])

        return None

    def find_prefix(self, prefix):
        """Counts the number of names matching the prefix"""

        if prefix == "":
            return 0

        subtree = self.find_subtree(self.tree, prefix)

        if not subtree:
            return 0

        return self.count_names(subtree)


if __name__ == "__main__":
    C = Contact()

    assert C.add_name("matt") == 1
    assert C.add_name("max") == 1
    assert C.add_name("matt") == 2

    pprint(C.tree)

    assert C.find_prefix("ma") == 3
